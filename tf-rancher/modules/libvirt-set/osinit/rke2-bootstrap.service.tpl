[Unit]
;ConditionFirstBoot=yes
ConditionFileIsExecutable=/usr/bin/rke2
ConditionPathExists=!/etc/systemd/system/multi-user.target.wants/rke2-${type}.service
Description=Enable RKE2
;Before=first-boot-complete.target systemd-machine-id-commit.service
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
RemainAfterExit=no
;ExecStart=systemctl daemon-reload
ExecStart=systemctl disable docker
ExecStart=systemctl stop docker
ExecStart=systemctl enable rke2-${type}
ExecStart=systemctl start rke2-${type}

[Install]
WantedBy=basic.target
