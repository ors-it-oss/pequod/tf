variable libvirt_uri {
  description = "Path to LibVirt API"
  default     = "qemu:///system"
  type        = string
  //  default = "qemu+ssh://deploys@ams-kvm-remote-host/system"
}

variable vms {
  description = "Number of VMs to instantiate"
  default     = 1
  type        = number
}

//variable vm_arch {
//  description = "VM arch"
//  default     = "x86_64"
//  type        = string
//}

variable vm_cores {
  description = "VM cores"
  default     = 4
  type        = number
}

variable vm_disk {
  description = "size of VM disk (MiB)"
  default     = 10240
  type        = number
}

variable vm_diskpool {
  description = "name of libvirt pool"
  default     = "default"
  type        = string
}

variable vm_net {
  description = "network for vms"
  default     = "default"
  type        = string
}

variable vm_ram {
  description = "VM RAM (MiB)"
  default     = 2048
  type        = number
}
//
//variable vm_tpl {
//  description = "VM template"
//  type        = string
//}

variable admin_user {
  description = "the ssh/admin user to use/create"
  default     = "orquestador"
  type        = string
}

variable ssh_key {
  description = "the private key to use"
  default     = "~/.ssh/id_rsa"
  type        = string
}

variable ssh_pub {
  description = "the public key"
  default     = "~/.ssh/id_rsa.pub"
  type        = string
}

variable cos_stream {
  default     = "stable"
  type        = string

  validation {
    condition     = contains(["stable", "testing", "next"], var.cos_stream)
    error_message = "Only stable, testing and next streams exist."
  }
}

variable rke2_channel {
  description = "RKE2 channel"
  default     = "latest"
  type        = string
}

variable rke2_role {
  description = "RKE2 role"
  default     = "worker"
  type        = string
  validation {
    condition     = contains(["server", "worker"], var.rke2_role)
    error_message = "Only servers & workers are allowed in RKE2."
  }
}

variable rke2_version {
  description = "RKE2 version"
  default     = "latest"
  type        = string
}
