terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  # Default provider for rest API to avoid error on destroy WRT uri argument
  # https://github.com/hashicorp/terraform/issues/21330
  uri = "qemu:///system"
}

module mgmt-set {
  source = "./modules/libvirt-set"
  vms = 3
  vm_cores = 2
  vm_ram = 1024
  rke2_role = "server"
  cos_stream = "stable"
}

//module worker-set {
//  source = "./modules/libvirt-set"
//  vms = 5
//  vm_cores = 4
//  vm_ram = 2048
//  rke2_role = "worker"
//  cos_stream = "testing"
//}
//
//module ingress-set {
//  source = "./modules/libvirt-set"
//  vms = 2
//  vm_cores = 2
//  vm_ram = 512
//  rke2_role = "worker"
//  cos_stream = "testing"
//}
