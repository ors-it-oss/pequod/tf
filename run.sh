#!/bin/bash
img=registry.gitlab.com/ors-it-oss/pequod/containers/ahab

set -a
TF_IN_AUTOMATION=true
TF_CLI_ARGS="-input=false"
TF_CLI_ARGS_apply="-auto-approve"
TF_CLI_ARGS_destroy="-auto-approve"
set +a

launch(){
	[[ -d $HOME/.terraform.d ]] || mkdir $HOME/.terraform.d
	name="pequod_$(date +%s)"
	podman run --rm -ti --name $name --userns=keep-id --read-only \
		--security-opt label=disable \
		--workdir /src \
		--tmpfs /run:noexec --tmpfs /tmp:noexec \
		${SSH_AUTH_SOCK:+--env SSH_AUTH_SOCK=/home/.ssh/auth.sock} \
		${SSH_AUTH_SOCK:+--volume $(readlink -f ${SSH_AUTH_SOCK}):/home/.ssh/auth.sock:z} \
		--env HOME=/home \
		--env TERM=xterm-256color \
		--volume $HOME/.ssh:/home/.ssh:ro \
		--volume $HOME/.terraform.d:/home/.terraform.d:rw \
		--volume /var/run/libvirt/libvirt-sock:/var/run/libvirt/libvirt-sock \
		--volume $PWD:/src:z \
	$img "$@"
}

clean(){
	tf destroy
	rm -rf $HOME/.terraform.d/ .terraform*
}

tf(){
	terraform "$*"
}

init(){
	install_libvirt
	tf init "$@"
}

install_libvirt(){
	# Next focus: Getting in registry <yesplz>
  tpath="$HOME/.terraform.d/plugins/registry.terraform.io/dmacvicar/libvirt/0.6.3/linux_amd64"
  mkdir -p "$tpath"
	url="https://github.com/dmacvicar/terraform-provider-libvirt/releases/download/v0.6.3/terraform-provider-libvirt-0.6.3+git.1604843676.67f4f2aa.Fedora_32.x86_64.tar.gz"
	curl -L "$url" | tar -C "$tpath" -xzvf -
}

if [[ -n "$1" ]]; then
	cmd=$1; shift
	case $cmd in
		apply|plan) tf $cmd "$@" ;;
		*) $cmd "$@" ;;
	esac
fi
