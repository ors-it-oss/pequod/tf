#!/bin/sh
set -x
set -eo pipefail

archive="$(basename $url)"

error_out(){
	echo "CAUGHT TRAP $?"
	rm -f ${image}* 2>/dev/null
	exit 1
}

sync_template(){
	if [[ -s "$image" ]]; then
		return 0
	elif [[ ! -s "$archive" ]]; then
		rm -f "$archive" 2>/dev/null
		curl -L -O "$url"
		echo "$checksum $archive" | sha256sum --check
	fi

	rm -f "$image" 2>/dev/null
	xz --verbose --threads=0 --decompress "$archive"
}

pwd
ls -al

sync_template || error_out
