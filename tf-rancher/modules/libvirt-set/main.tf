terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
    ignition = {
      source = "community-terraform-providers/ignition"
    }
  }
}

provider libvirt {
  uri = var.libvirt_uri
}

module cos_tpl {
  source = "../coreos"
  stream = var.cos_stream
  platform = "qemu"
}

resource local_file qcow {
  // Cache, checksum & extract the CoreOS template locally
  filename = "${path.root}/.terraform/cache/${module.cos_tpl.image}.cache"
  provisioner local-exec {
    environment = {
      checksum = module.cos_tpl.checksum
      image = module.cos_tpl.image
      url = module.cos_tpl.url
    }
    working_dir = dirname(self.filename)
    command = file("${path.module}/local/tpl.sh")
  }
}

resource libvirt_volume image {
  // Send extracted template to image store
  name = module.cos_tpl.image
  source = "${dirname(local_file.qcow.filename)}/${module.cos_tpl.image}"
  pool = var.vm_diskpool
  format = "qcow2"
}


########-------- VM set ----------------################
resource random_pet hostname {
  //  keepers = {
  //    # Generate a new pet name each time we switch to a new AMI id
  //    ami_id = "${var.ami_id}"
  //  }
  count = var.vms

}

resource libvirt_volume disk {
  name = "${element(random_pet.hostname.*.id, count.index)}.qcow2"
  count = var.vms
  base_volume_name = libvirt_volume.image.name
  pool = var.vm_diskpool
  format = "qcow2"
  size = var.vm_disk * pow(1024, 2)
}

resource libvirt_ignition ignition {
  name = "${element(random_pet.hostname.*.id, count.index)}.ign"
  pool = var.vm_diskpool
  count = var.vms
  content = element(data.ignition_config.ignition.*.rendered, count.index)
  depends_on = [
    libvirt_volume.disk,
  ]
}

resource libvirt_domain vm {
  count = var.vms
  name = element(random_pet.hostname.*.id, count.index)
  vcpu = 2
  memory = 1024
  running = true
  qemu_agent = true
  firmware = "/usr/share/edk2/ovmf/OVMF_CODE.fd"
  machine = "q35"

  cpu = {
    mode = "host-passthrough"
  }

  network_interface {
    network_name = var.vm_net
    //    mac            = "52:54:00:00:00:a${count.index + 1}"
    wait_for_lease = true
    hostname = element(random_pet.hostname.*.id, count.index)
  }

  console {
    type = "pty"
    target_port = 0
    target_type = "serial"
  }

  //  console {
  //    type        = "pty"
  //    target_type = "virtio"
  //    target_port = "1"
  //  }

  disk {
    volume_id = element(libvirt_volume.disk.*.id, count.index)
    scsi = true
  }

  coreos_ignition = element(libvirt_ignition.ignition.*.id, count.index)
  //  fw_cfg_name = "opt/org.flatcar-linux/config"
  fw_cfg_name = "opt/com.coreos/config"
}
