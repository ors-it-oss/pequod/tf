output checksum {
  value = local.downloads.sha256
}

output image {
  value = replace(basename(local.downloads.location), "/.(xz|gz)$/", "")
}

output release {
  value = local.stream.release
}

output signature {
  value = local.downloads.signature
}

output url {
  value = local.downloads.location
}
