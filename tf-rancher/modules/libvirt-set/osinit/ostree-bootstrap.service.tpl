[Unit]
ConditionFirstBoot=yes
Description=Install additional packages
Before=first-boot-complete.target
;systemd-machine-id-commit.service
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
RemainAfterExit=no
ExecStart=rpm-ostree install ${join(" ", packages)}
ExecStart=/bin/systemctl --no-block reboot

[Install]
WantedBy=basic.target
