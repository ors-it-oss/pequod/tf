data http stream {
  url = "https://builds.coreos.fedoraproject.org/streams/${var.stream}.json"
  request_headers = {
    Accept = "application/json"
  }
}

locals {
  stream = jsondecode(data.http.stream.body).architectures.x86_64.artifacts[var.platform]
  downloads = values(local.stream.formats)[0].disk
}
