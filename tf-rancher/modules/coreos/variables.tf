variable stream {
  description = "CoreOS Stream to use"
  default     = "stable"
  type        = string

  validation {
    condition     = contains(["stable", "testing", "next"], var.stream)
    error_message = "Only stable, testing and next streams exist."
  }
}

variable platform {
  description = "Platform to return stream info for"
  default = "qemu"

  validation {
    condition = contains([
      "aliyun","aws","azure",
      "digitalocean","exoscale",
      "gcp","ibmcloud","metal","openstack",
      "qemu","vmware","vultr"
    ], var.platform)
    error_message = "Invalid platform for CoreOS."
  }
}
