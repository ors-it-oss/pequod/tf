[rancher-rke2-common-${channel}]
name=Rancher RKE2 Common (${channel})
baseurl=https://${server}/rke2/${channel}/common/centos/8/noarch
enabled=1
gpgcheck=1
gpgkey=https://${server}/public.key

[rancher-rke2-${version}-${channel}]
name=Rancher RKE2 ${version} (${channel})
baseurl=https://${server}/rke2/${channel}/${version}/centos/8/$basearch
enabled=1
gpgcheck=1
gpgkey=https://${server}/public.key
