data ignition_config ignition {
  users = [
    data.ignition_user.os_admin.rendered
  ]

  files = [
    data.ignition_file.rke2-dnf-repo.rendered,
    data.ignition_file.rke2-profile.rendered,
    data.ignition_file.rke2-config.rendered,
    element(data.ignition_file.hostname.*.rendered, count.index)
  ]

//  networkd = [
//    data.ignition_networkd_unit.network-dhcp.rendered,
//  ]

  systemd = [
    data.ignition_systemd_unit.ostree-bootstrap.rendered,
    data.ignition_systemd_unit.rke2-bootstrap[count.index].rendered,
  ]

  count = var.vms
}

data ignition_file hostname {
  path       = "/etc/hostname"
  mode       = 420
  content {
    content = element(random_pet.hostname.*.id, count.index)
  }
  count = var.vms
}

data ignition_user os_admin {
  name = var.admin_user
  groups = [
    "docker",
    "sudo"
  ]
  ssh_authorized_keys = [
    file(pathexpand(var.ssh_pub))
  ]
}

//data "ignition_networkd_unit" "network-dhcp" {
//  name    = "00-wired.network"
//  content = file("${path.module}/vmdata/one.yml")
//}
//

data ignition_systemd_unit ostree-bootstrap {
  name = "ostree-bootstrap.service"
  enabled = true
  content = templatefile("${path.module}/osinit/ostree-bootstrap.service.tpl", {
    packages = [
      "qemu-guest-agent",
      "rke2-server"
    ]
  })
}

//resource random_string token {
//  length  = 16
//  special = false
//}

data ignition_systemd_unit rke2-bootstrap {
  name    = "rke2-bootstrap.service"
  enabled = true
  content = templatefile("${path.module}/osinit/rke2-bootstrap.service.tpl", {
    type          = "server"
  })
  count = var.vms
}

//data template_file install-rke2 {
//  template = file("${path.module}/systemd/install-rke2.service.tpl")
//  count    = var.vms
//  vars = {
//    channel       = var.rke2_channel
//    type          = "server"
//  }
//}

//data ignition_file get-rke2-io {
//  path       = "/usr/local/sbin/rke2-install"
//  mode       = 493
//  source {
//    source = "https://get.rke2.io"
//  }
//}

data ignition_file rke2-profile {
  path       = "/etc/profile.d/rancher-rke2.sh"
  mode       = 420
  content {
    content = "export PATH=$PATH:/var/lib/rancher/rke2/bin"
  }
}

data ignition_file rke2-dnf-repo {
  path       = "/etc/yum.repos.d/rancher-rke2.repo"
  mode       = 420
  content {
    content = templatefile("${path.module}/osinit/rancher-rke2.repo.tpl", {
      server = "rpm.rancher.io"
      channel = var.rke2_channel
      version = var.rke2_version == "latest" ? regex("^v([0-9]+.[0-9]+)", jsondecode(data.http.rke2-release.body).tag_name)[0] : var.rke2_version
    })
  }
}

data http rke2-release {
  url = "https://update.rke2.io/v1-release/channels/${var.rke2_channel}"
  request_headers = {
    Accept = "application/json"
  }
}

data ignition_file rke2-config {
  path       = "/etc/rancher/rke2/config.yaml"
  mode       = 420
  content {
    content = templatefile("${path.module}/osinit/rke2-config-server.yml", {})
  }
}
